#+title: Testing Arei, a New Guile Scheme IDE for Emacs
#+subtitle: System Crafters Live - December 22, 2023
#+date: [2023-12-22 Fri]
#+video: qmHU3UGvXgo

* Updates

- Happy Holidays!

* Let's try out a new Guile Scheme IDE, Arei

- https://git.sr.ht/~abcdw/emacs-arei
- https://git.sr.ht/~abcdw/guile-ares-rs
- https://emacsconf.org/2023/talks/scheme/

** Guix Configuration

#+begin_src scheme

#+end_src
